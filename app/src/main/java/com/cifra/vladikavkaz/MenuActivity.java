package com.cifra.vladikavkaz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.cifra.vladikavkaz.Delegate.OnResumeDelegate;
import com.cifra.vladikavkaz.Fragments.FragmentExtanded;
import com.cifra.vladikavkaz.Fragments.InitiativesFragment;
import com.cifra.vladikavkaz.Fragments.MenuFragment;
import com.cifra.vladikavkaz.Fragments.NotificationFragment;
import com.cifra.vladikavkaz.Fragments.ProfileFragment;
import com.cifra.vladikavkaz.Fragments.QuestionsFragment;
import com.cifra.vladikavkaz.Fragments.RequestsFragment;
import com.cifra.vladikavkaz.Models.ProfileModel;
import com.cifra.vladikavkaz.Models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MenuActivity extends AppCompatActivity {

    SharedPreferences pref;
    public String userPhone;
    String userCode;
    TextView tvHeader;
    FrameLayout frame;
    ProfileModel profileModel;
    String karakuli;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        super.onCreate(savedInstanceState);

        verifyAuthorizationData();
        setContentView(R.layout.menu_activity);

        frame = findViewById(R.id.mainFrame);

        tvHeader = findViewById(R.id.header);

        push(new MenuFragment(), true);
        push(new MenuFragment());

        readProfileData(userPhone);
    }

    private void readProfileData(String phone){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("profiles/" + phone);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                profileModel = dataSnapshot.getValue(ProfileModel.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void menuItemClick(View view){
        String value = "";
        FragmentExtanded tmp = null;

        switch (view.getId()) {
            case R.id.profile:
                tmp = new ProfileFragment(profileModel, userPhone);
                break;

            case R.id.notification:
                tmp = new NotificationFragment();
                break;

            case R.id.initiative:
                tmp = new InitiativesFragment();
                break;
            case R.id.question:
                tmp = new QuestionsFragment();
                break;

            case R.id.request:
                tmp = new RequestsFragment();
                break;

            case R.id.call:
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + "+79094722223"));
                startActivity(intent);
                break;
        }

                if (tmp != null){
                    push(tmp, true);
                }
    }

    private void verifyAuthorizationData(){
        userCode = pref.getString("userCode", null);
        userPhone = pref.getString("userPhone", null);

        if (userCode == null || userPhone == null){
            openLoginActivity();
        }

        endIfAuthorizationDataIncorrect(userPhone, userCode);
    }

    private void endIfAuthorizationDataIncorrect(String phone, final String code){
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("users");

        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> usersIterable = dataSnapshot.getChildren();
                DataSnapshot dsUser;

                while (usersIterable.iterator().hasNext()){
                    dsUser = usersIterable.iterator().next();
                    User user = dsUser.getValue(User.class);
                    if (user != null && user.code.equals(code)){
                        karakuli = dsUser.getKey();
                        return;
                    }
                }

                runOnUiThread(toLoginActivity);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        dbRef = dbRef.orderByChild("phone").equalTo(phone).getRef();
        dbRef.orderByChild("date").limitToLast(1);
        dbRef.addValueEventListener(listener);
    }

    public void push(final FragmentExtanded fragment){
        fragment.setOnResumeDelegate(onFragmentResume);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        fragmentTransaction.replace(R.id.mainFrame, fragment);
        fragmentTransaction.commit();

    }
    public void push(FragmentExtanded fragment, boolean addToBack){
        fragment.setOnResumeDelegate(onFragmentResume);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrame, fragment);
        if (addToBack)
            fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        tvHeader.setText(fragment.getHeader());
    }

    private void openLoginActivity(){
        Intent loginActivity = new Intent(this, LoginActivity.class);
        loginActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(loginActivity);

        finish();
    }

    public void logout(View view){
        toLoginActivity.run();
    }

    private void removePreference(){
        SharedPreferences.Editor edit = pref.edit();
        edit.remove("userCode");
        edit.remove("userPhone");
        edit.commit();
    }

    Runnable toLoginActivity = new Runnable() {
        @Override
        public void run() {
            removePreference();
            openLoginActivity();
        }
    };

    OnResumeDelegate onFragmentResume = new OnResumeDelegate() {
        @Override
        public void onResume(String header) {
                tvHeader.setText(header);
        }
    };


}
