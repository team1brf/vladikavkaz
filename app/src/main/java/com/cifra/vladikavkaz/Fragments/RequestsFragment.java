package com.cifra.vladikavkaz.Fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cifra.vladikavkaz.Adapters.RequestsAdapter;
import com.cifra.vladikavkaz.Delegate.OnResumeDelegate;
import com.cifra.vladikavkaz.MenuActivity;
import com.cifra.vladikavkaz.Models.RequestModel;
import com.cifra.vladikavkaz.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class RequestsFragment extends FragmentExtanded {
    private String header = "Обращения";
    RecyclerView rvRequests;
    RequestsAdapter adapter;
    ArrayList<RequestModel> requests;
    private OnResumeDelegate onResumeDelegate;
    private ProgressBar progress;
    private FloatingActionButton floatingButton;
    private SharedPreferences pref;
    private String phone;

    private DatabaseReference reference;
    private ValueEventListener valueEventListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_requests, container, false);

        progress = view.findViewById(R.id.progress);
        floatingButton = view.findViewById(R.id.btnAddRequest);
        pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        phone = pref.getString("userPhone", null);
        floatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MenuActivity)getContext()).push(new AddRequestFragment(), true);
            }
        });
        requests = new ArrayList<>();
        RequestModel model = getTmpRequest();
        requests.add(model);
        requests.add(model);
        adapter = new RequestsAdapter(requests, getContext());

        rvRequests = view.findViewById(R.id.requestsRView);
//        rvRequests.setLayoutManager(new LinearLayoutManager(view.getContext()));
//        rvRequests.setAdapter(adapter);

        updateDate();

        return view;
    }

    void setAdapter( ArrayList<RequestModel> notifications){
        adapter = new RequestsAdapter(notifications, getContext());
        rvRequests.setLayoutManager(new LinearLayoutManager(getContext()));
        rvRequests.setAdapter(adapter);
    }

    private void updateDate(){
        progress.setVisibility(View.VISIBLE);

        if (reference == null){
            reference = FirebaseDatabase.getInstance().getReference("appeals");
            valueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    progress.setVisibility(View.GONE);
                    requests = new ArrayList<>();

                    for (DataSnapshot dsNotification : dataSnapshot.getChildren()) {
                        RequestModel request = dsNotification.getValue(RequestModel.class);
                        if (request == null || !request.author.equals(phone)){
                            continue;
                        }
                        request.id = dsNotification.getKey();
                        requests.add(request);
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setAdapter(requests);
                        }
                    });
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    progress.setVisibility(View.GONE);

                }
            };

            reference.addValueEventListener(valueEventListener);
        }

    }

    @Override
    public String getHeader() {
        return header;
    }

    @Override
    public void setOnResumeDelegate(OnResumeDelegate delegate) {
        onResumeDelegate = delegate;
    }

    @Override
    public void onResume() {
        super.onResume();
//        onResumeDelegate.onResume(header);
    }

    private RequestModel getTmpRequest(){
        RequestModel tmpRequest = new RequestModel();
        tmpRequest.date = "12.12.1222";
        tmpRequest.status = "жду";
        tmpRequest.topic = "какая-то тема";
        tmpRequest.text = "Текст";
        return tmpRequest;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        reference.removeEventListener(valueEventListener);
    }
}
