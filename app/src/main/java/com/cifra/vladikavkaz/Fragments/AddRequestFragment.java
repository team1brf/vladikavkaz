package com.cifra.vladikavkaz.Fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.cifra.vladikavkaz.Delegate.OnResumeDelegate;
import com.cifra.vladikavkaz.MenuActivity;
import com.cifra.vladikavkaz.Models.AppealTopic;
import com.cifra.vladikavkaz.Models.Organization;
import com.cifra.vladikavkaz.Models.RequestModel;
import com.cifra.vladikavkaz.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AddRequestFragment extends FragmentExtanded{

    private LinearLayout llContainer;
    private ProgressBar progress;
    private String header = "Обращение";
    private OnResumeDelegate onResumeDelegate;
    private Button btnSend;
    private Spinner spinnerTopic;
    private Spinner spinnerOrganization;
    private EditText etText;

    private Boolean isTopics = false;
    private Boolean isOrganizations = false;

    private AppealTopic topicValue = null;
    private String organizationIdValue = null;

    private List<AppealTopic> appealsTopics;
    private List<String> appealsTopicsValues;
    private List<String> organizations;
    private List<String> organizationsValues;

    private DatabaseReference ref;
    private ValueEventListener valueEventListener;

    private DatabaseReference refTopic;
    private ValueEventListener valueEventListenerTopic;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_request, container, false);
        init(view);


        spinnerTopic.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                topicValue = appealsTopics.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinnerOrganization.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                organizationIdValue = organizations.get(i);
                topicValue = appealsTopics.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendRequest(topicValue, organizationIdValue);

            }
        });

        return view;
    }

    private void sendRequest(AppealTopic topic, String organization){
        String text = etText.getText().toString();
        if (text.isEmpty()){
            return;
        }

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("appeals");
        ref = ref.push();

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        String phone = pref.getString("userPhone", null);

        Date d = new Date();
        RequestModel requestModel = new RequestModel();
        requestModel.approved = "false";
        requestModel.answer = "";
        requestModel.author = phone;
        requestModel.text= text;
        requestModel.date = DateFormat.format("d MMMM yyyy ", d.getTime()).toString();
        requestModel.organization = organization;
        requestModel.status = "на рассмотрении";
        requestModel.topic = topic.id;

        Task<Void> task = ref.setValue(requestModel);
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                ((MenuActivity)getContext()).getSupportFragmentManager().popBackStack();
            }
        });


    }

    private void init(View view){
        llContainer = view.findViewById(R.id.llContainer);
        btnSend = view.findViewById(R.id.btnSend);
        spinnerTopic = view.findViewById(R.id.sTopic);
        spinnerOrganization = view.findViewById(R.id.sOrganization);
        etText= view.findViewById(R.id.textValue);
        progress = view.findViewById(R.id.progress);


        progress.setVisibility(View.VISIBLE);
        getTopics();
        getOrganizations();
    }

    private void getTopics(){
        appealsTopics = new ArrayList<>();
        appealsTopicsValues = new ArrayList<>();

        if (refTopic == null){
            refTopic = FirebaseDatabase.getInstance().getReference("appealsTopic");
            valueEventListenerTopic = new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        String currTopic = dataSnapshot1.getValue(String.class);
                        appealsTopicsValues.add(currTopic);
                        appealsTopics.add(new AppealTopic(dataSnapshot.getKey(), currTopic));
                    }
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, appealsTopicsValues);
                    dataAdapter.setDropDownViewResource(R.layout.simple_custom_dropdown_item);
                    spinnerTopic.setAdapter(dataAdapter);
                    isTopics = true;
                    showContainer();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            };

            refTopic.addValueEventListener(valueEventListenerTopic);
        }

    }

    private void getOrganizations(){
        organizations = new ArrayList<>();
        organizationsValues = new ArrayList<>();

        if (ref == null){
            ref = FirebaseDatabase.getInstance().getReference("organizations");
            valueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        Organization currOrganization = dataSnapshot1.getValue(Organization.class);
                        if (currOrganization != null) {
                            organizationsValues.add(currOrganization.organization);
                            String id = dataSnapshot1.getKey();
                            organizations.add(id);
                        }
                    }

                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, organizationsValues);
                    dataAdapter.setDropDownViewResource(R.layout.simple_custom_dropdown_item);
                    spinnerOrganization.setAdapter(dataAdapter);
                    isOrganizations = true;
                    showContainer();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            };

            ref.addValueEventListener(valueEventListener);
        }
    }

    private void showContainer(){
        if (isTopics && isOrganizations){
            llContainer.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);
        }
    }

    @Override
    public String getHeader() {
        return header;
    }

    @Override
    public void setOnResumeDelegate(OnResumeDelegate delegate) {
        onResumeDelegate = delegate;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (onResumeDelegate == null){
            onResumeDelegate.onResume(header);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (ref != null){
            ref.removeEventListener(valueEventListener);
        }

        if (refTopic != null){
            refTopic.removeEventListener(valueEventListenerTopic);
        }
    }
}
