package com.cifra.vladikavkaz.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.cifra.vladikavkaz.Delegate.OnResumeDelegate;
import com.cifra.vladikavkaz.Models.NotificationModel;
import com.cifra.vladikavkaz.R;

public class NotificatonDetailFragment extends FragmentExtanded {

    private String header = "Опповещения";
    private OnResumeDelegate onResumeDelegate;
    private NotificationModel notificationModel;

    public NotificatonDetailFragment(NotificationModel notificationModel) {
        this.notificationModel = notificationModel;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification_detail, container, false);

        return view;
    }

    @Override
    public String getHeader() {
        return header;
    }

    @Override
    public void setOnResumeDelegate(OnResumeDelegate delegate) {
        onResumeDelegate = delegate;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (onResumeDelegate != null){
            onResumeDelegate.onResume(header);
        }
    }
}
