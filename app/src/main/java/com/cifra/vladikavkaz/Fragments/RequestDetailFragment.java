package com.cifra.vladikavkaz.Fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.cifra.vladikavkaz.Delegate.OnResumeDelegate;
import com.cifra.vladikavkaz.MenuActivity;
import com.cifra.vladikavkaz.Models.RequestModel;
import com.cifra.vladikavkaz.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;

public class RequestDetailFragment extends FragmentExtanded{

    private String header;
    private OnResumeDelegate onResumeDelegate;
    private RequestModel requestModel;

    private TextView tvTopicValue;
    private TextView tvStatusValue;
    private TextView tvTextValue;
    private TextView tvDate;
    private LinearLayout llButtons;
    private String phone;
    private SharedPreferences pref;
    private Button btnAccept;
    private Button btnRefuse;

    public RequestDetailFragment(RequestModel requestModel) {
        this.requestModel = requestModel;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_request_detail, container, false);

        init(view);
        setData();

        return view;
    }

    private void init(View view){
        tvTopicValue = view.findViewById(R.id.topicValue);
        tvStatusValue = view.findViewById(R.id.statusValue);
        tvTextValue = view.findViewById(R.id.textValue);
        tvDate = view.findViewById(R.id.tvDate);
        llButtons = view.findViewById(R.id.llButtons);
        pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        phone = pref.getString("userPhone", null);

        btnAccept = view.findViewById(R.id.accept);
        btnRefuse = view.findViewById(R.id.refuse);

        if (requestModel.status != null && !requestModel.status.isEmpty() && requestModel.author.equals(phone)){
            llButtons.setVisibility(View.VISIBLE);
            btnRefuse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setApproved(requestModel, false);
                }
            });
            btnAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setApproved(requestModel, true);
                }
            });
        }

    }

    private void setApproved(RequestModel requestModel, Boolean value){
        String approvedValue = value.toString();
        Task<Void> task = FirebaseDatabase.getInstance().getReference("appeals/" + requestModel.id + "/approved").setValue(approvedValue);
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                ((MenuActivity)getContext()).getSupportFragmentManager().popBackStack();
            }
        });

    }

    private void setData(){
        tvTopicValue.setText(requestModel.topic);
        tvStatusValue.setText(requestModel.status);
        tvTextValue.setText(requestModel.text);
        tvDate.setText("Обращение от " + requestModel.date);
    }

    @Override
    public String getHeader() {
        return header;
    }

    @Override
    public void setOnResumeDelegate(OnResumeDelegate delegate) {
        onResumeDelegate = delegate;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (onResumeDelegate != null){
            onResumeDelegate.onResume(header);
        }
    }
}
