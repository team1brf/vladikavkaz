package com.cifra.vladikavkaz.Fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.cifra.vladikavkaz.Adapters.QestionsAdapter;
import com.cifra.vladikavkaz.Delegate.OnResumeDelegate;
import com.cifra.vladikavkaz.MenuActivity;
import com.cifra.vladikavkaz.Models.InitiativeModel;
import com.cifra.vladikavkaz.Models.OrganizationClass;
import com.cifra.vladikavkaz.Models.QuestionModel;
import com.cifra.vladikavkaz.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddInitiativeFragment extends FragmentExtanded {

    Spinner topicList;
    Spinner whereList;
    EditText etInititative;
    RecyclerView answerList;
    RelativeLayout addAnswer;
    QestionsAdapter adapter;
    ArrayList<QuestionModel> items;
    OnResumeDelegate onResumeDelegate;
    ArrayList<String> orgs = new ArrayList<>();
    SpinnerAdapter spinnerAdapter;
    ImageButton addAnswerBtn;
    Button sendBtn;




    public AddInitiativeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference db = mDatabase.child("organizations");
        spinnerAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, orgs);

        db.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                OrganizationClass org =  dataSnapshot.getValue(OrganizationClass.class);
                orgs.add(org.organization);
                spinnerAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, orgs);
                whereList.setAdapter(spinnerAdapter);
                whereList.setSelection(0);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        View v = inflater.inflate(R.layout.fragment_add_initiative, container, false);
        topicList = v.findViewById(R.id.topicList);
        whereList = v.findViewById(R.id.whereList);
        etInititative = v.findViewById(R.id.etInititative);
        answerList = v.findViewById(R.id.answerList);
        addAnswer = v.findViewById(R.id.addAnswer);
        addAnswerBtn = v.findViewById(R.id.addAnswerBtn);
        items = new ArrayList<>();
        whereList.setAdapter(spinnerAdapter);
        adapter = new QestionsAdapter(items);
        answerList.setLayoutManager(new LinearLayoutManager(getContext()));
        answerList.setAdapter(adapter);
        sendBtn = v.findViewById(R.id.sendBtn);
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InitiativeModel model = new InitiativeModel();
                model.text = etInititative.getText().toString();
                model.date = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
                model.org = orgs.get(whereList.getSelectedItemPosition());
                model.author = ((MenuActivity) getActivity()).userPhone;
                model.answers = new ArrayList<>();
                DatabaseReference db = mDatabase.child("initiatives");
                DatabaseReference ref = db.push();
                ref.setValue(model);

                int k = 0;
                for (QuestionModel q : items){
                    DatabaseReference key = ref.child("answers");
                    if (q.count != null && !q.count.equals("")) {
                        key.child(k + "/count").setValue(q.count);
                    } else {
                        key.child(k + "/count").setValue("0");
                    }
                    key.child(k+"/text").setValue(q.text);
                    k++;
                }
                getActivity().onBackPressed();
            }
        });
        addAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                items.add(new QuestionModel());
                adapter.notifyDataSetChanged();
            }
        });
        addAnswerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                items.add(new QuestionModel());
                adapter.notifyDataSetChanged();
            }
        });
        return v;
    }

    @Override
    public String getHeader() {
        return "Инициативы";
    }


    @Override
    public void setOnResumeDelegate(OnResumeDelegate delegate) {
        onResumeDelegate = delegate;
    }

    @Override
    public void onResume() {
        super.onResume();
        onResumeDelegate.onResume("Инициативы");
    }
}
