package com.cifra.vladikavkaz.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cifra.vladikavkaz.Adapters.NotificationsAdapter;
import com.cifra.vladikavkaz.Delegate.OnResumeDelegate;
import com.cifra.vladikavkaz.Models.NotificationModel;
import com.cifra.vladikavkaz.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

public class NotificationFragment extends FragmentExtanded {

    private ProgressBar progress;
    private OnResumeDelegate onResumeDelegate;
    private String header = "Оповещения";
    private ArrayList<NotificationModel> notifications;
    private RecyclerView rvNotifications;
    private NotificationsAdapter adapter;

    private DatabaseReference reference;
    private ValueEventListener valueEventListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);

        rvNotifications = view.findViewById(R.id.notificationRView);
        progress = view.findViewById(R.id.progress);

        updateDate();

        return view;
    }

    void setAdapter( ArrayList<NotificationModel> notifications){
        adapter = new NotificationsAdapter(notifications, getContext());
        rvNotifications.setLayoutManager(new LinearLayoutManager(getContext()));
        rvNotifications.setAdapter(adapter);
    }

    private void updateDate(){
        progress.setVisibility(View.VISIBLE);

        if (reference != null){
            return;
        }

        reference = FirebaseDatabase.getInstance().getReference("alerts");
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progress.setVisibility(View.GONE);
                notifications = new ArrayList<>();

                for (DataSnapshot dsNotification : dataSnapshot.getChildren()) {
                    NotificationModel notification = dsNotification.getValue(NotificationModel.class);
                    notifications.add(notification);
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setAdapter(notifications);
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progress.setVisibility(View.GONE);

            }
        };
        reference.addValueEventListener(valueEventListener);
    }

    @Override
    public String getHeader() {
        return header;
    }

    @Override
    public void setOnResumeDelegate(OnResumeDelegate delegate) {
        onResumeDelegate = delegate;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (onResumeDelegate != null) {
            onResumeDelegate.onResume(header);
        }
    }

    private NotificationModel getTmpMpdel(){
        NotificationModel model = new NotificationModel();
        model.author = "Администрация местного самоуправления";
        model.date = "12.10.2019 12:00";
        model.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";
        model.title = "C 22 июня по 27 февраля будет перекрыто движение по ул Ватутина от СОГУ до Дружбы";
        model.type = "alert";
        model.visible = "true";

        return model;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (reference != null){
            reference.removeEventListener(valueEventListener);
        }
    }
}
