package com.cifra.vladikavkaz.Fragments;

import androidx.fragment.app.Fragment;

import com.cifra.vladikavkaz.Delegate.OnResumeDelegate;

public abstract class FragmentExtanded extends Fragment {
    public abstract String getHeader();
    public abstract void setOnResumeDelegate(OnResumeDelegate delegate);

}
