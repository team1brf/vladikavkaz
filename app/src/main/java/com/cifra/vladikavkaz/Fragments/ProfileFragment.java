package com.cifra.vladikavkaz.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.cifra.vladikavkaz.Delegate.OnResumeDelegate;
import com.cifra.vladikavkaz.MenuActivity;
import com.cifra.vladikavkaz.Models.ProfileModel;
import com.cifra.vladikavkaz.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

public class ProfileFragment extends FragmentExtanded {

    private String header = "Мой профиль";
    private OnResumeDelegate onResumeDelegate;
    private EditText etPhone;
    private EditText etFirstName;
    private EditText etSecondName;
    private EditText etLastName;
    private EditText etStreet;
    private EditText etHouse;
    private EditText etRoom;

    private Button btnSave;

    private String phone;

    private ProfileModel profile;

    public ProfileFragment(ProfileModel profile, String phone) {
        if (profile == null){
            profile = new ProfileModel();
        }

        checkUser(profile);
        this.profile = profile;
        this.phone = phone;
    }

    private void checkUser(ProfileModel profile){
        profile.apt = profile.apt == null ? "" : profile.apt ;
        profile.fam = profile.fam == null ? "" : profile.fam ;
        profile.house = profile.house == null ? "" : profile.house ;
        profile.mdlname = profile.mdlname == null ? "" : profile.mdlname ;
        profile.street = profile.street == null ? "" : profile.street ;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        setViewComponents(view);
        updateViewComponents(profile);


        MaskedTextChangedListener listener = new MaskedTextChangedListener("+7 ([000]) [000]-[00]-[00]", etPhone);
        etPhone.addTextChangedListener(listener);
        etPhone.setHint(listener.placeholder());

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveUserData();
            }
        });

        return view;
    }

    private void saveUserData(){
        profile.house = etHouse.getText().toString();
        profile.apt = etRoom.getText().toString();
        profile.street = etStreet.getText().toString();
        profile.fam = etSecondName.getText().toString();
        profile.name = etFirstName.getText().toString();
        profile.mdlname = etLastName.getText().toString();

        Task<Void> task = FirebaseDatabase.getInstance().getReference("profiles/" + phone).setValue(profile);
        task.addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(getContext(), "Сохранено", Toast.LENGTH_LONG).show();
                ((MenuActivity)getContext()).getSupportFragmentManager().popBackStack();
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.w("","");

            }
        });
    }

    private void closeFragment(){
        
    }

    private void updateViewComponents(ProfileModel model){
        etPhone.setText(phone);
        etFirstName.setText(model.name == null ? "" : model.name);
        etHouse.setText(model.house == null ? "" : model.house);
        etSecondName.setText(model.fam == null ? "" : model.fam);
        etLastName.setText(model.mdlname == null ? "" : model.mdlname);
        etStreet.setText(model.street == null ? "" : model.street);
        etRoom.setText(model.apt == null ? "" : model.apt);
    }

    private void setViewComponents(View view){
        etPhone = view.findViewById(R.id.phone);
        etFirstName = view.findViewById(R.id.firstName);
        etSecondName = view.findViewById(R.id.secondName);
        etLastName = view.findViewById(R.id.lastName);
        etStreet = view.findViewById(R.id.street);
        etHouse = view.findViewById(R.id.house);
        etRoom = view.findViewById(R.id.room);

        btnSave = view.findViewById(R.id.profileSave);
    }

    @Override
    public String getHeader() {
        return header;
    }

    @Override
    public void setOnResumeDelegate(OnResumeDelegate delegate) {
        onResumeDelegate = delegate;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (onResumeDelegate != null){
            onResumeDelegate.onResume(header);
        }
    }
}
