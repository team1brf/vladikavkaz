package com.cifra.vladikavkaz.Fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cifra.vladikavkaz.Adapters.MsgAdapter;
import com.cifra.vladikavkaz.Delegate.OnResumeDelegate;
import com.cifra.vladikavkaz.Models.Message;
import com.cifra.vladikavkaz.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class QuestionsFragment extends FragmentExtanded {

    private RecyclerView recycler;
    private EditText etMessage;
    private ImageView btnSend;
    private String header = "Задать вопрос";
    private OnResumeDelegate onResumeDelegate;
    private List<Message> messages;
    private String phone;
    private SharedPreferences pref;
    private MsgAdapter adapter;
    private DatabaseReference ref;
    private ValueEventListener valueEventListener;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_questions, container, false);

        init(view);

        return view;
    }


    private void init(View view){
        recycler = view.findViewById(R.id.recycler);
        etMessage = view.findViewById(R.id.etMessage);
        btnSend = view.findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                String message = etMessage.getText().toString();
                sendMessage(message);
                etMessage.setText("");
                etMessage.clearFocus();
            }
        });
        getPhone();
        getMessages();
    }

    private void hideKeyboard(){

        InputMethodManager imm = (InputMethodManager) etMessage.getContext().getSystemService(getContext().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etMessage.getWindowToken(), 0);


        View v = getActivity().getCurrentFocus();
        if (v instanceof EditText) {
            v.clearFocus();
            InputMethodManager im = (InputMethodManager) getContext().getSystemService(getContext().INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    private void getPhone(){
        pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        phone = pref.getString("userPhone", null);
    }

    private void getMessages(){
        if (messages == null){
            messages = new ArrayList<>();
            ref = FirebaseDatabase.getInstance().getReference("questions");
            valueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    messages.clear();
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        Message currMessage = dataSnapshot1.getValue(Message.class);
                        if (currMessage == null){
                            continue;
                        }

                        if (currMessage.author.equals(phone)) {
                            currMessage.setIs_income(false);
                        } else {
                            currMessage.setIs_income(true);
                        }
                        messages.add(currMessage);
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setAdapter(messages);
                        }
                    });
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            };
        }else {
            messages.clear();
        }

        ref.addValueEventListener(valueEventListener);
    }

    private void sendMessage(final String message){
        if (message == null || message.isEmpty()){
            return;
        }

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("questions").push();
        Message currMessage = getMessage(message);
        ref.setValue(currMessage);

        messages.add(currMessage.dbObject());
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
                recycler.scrollToPosition(messages.size() - 1);
            }
        });
    }

    private Message getMessage(String message){
        Message msg = new Message();
        Date d = new Date();

        msg.author = phone;
        msg.date = DateFormat.format("d MMMM yyyy ", d.getTime()).toString();
        msg.text = message;
        return msg;
    }

    private void setAdapter(List<Message> list){
        if (adapter == null){
            adapter = new MsgAdapter(getContext(), list);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());

            recycler.setLayoutManager(layoutManager);
            recycler.setAdapter(adapter);
        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
                ((LinearLayoutManager)recycler.getLayoutManager()).scrollToPosition(messages.size() - 1);
            }
        });


    }

    @Override
    public String getHeader() {
        return header;
    }

    @Override
    public void setOnResumeDelegate(OnResumeDelegate delegate) {
        onResumeDelegate = delegate;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (onResumeDelegate != null){
            onResumeDelegate.onResume(header);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ref.removeEventListener(valueEventListener);
    }
}
