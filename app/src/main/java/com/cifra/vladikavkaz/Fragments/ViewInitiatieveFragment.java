package com.cifra.vladikavkaz.Fragments;


import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.cifra.vladikavkaz.Delegate.OnResumeDelegate;
import com.cifra.vladikavkaz.Models.InitiativeModel;
import com.cifra.vladikavkaz.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewInitiatieveFragment extends FragmentExtanded {

    private OnResumeDelegate onResumeDelegate;
    private InitiativeModel item;
    private TextView title;
    private TextView text;
    private RadioGroup radioGroup;

    private DatabaseReference db;
    private ChildEventListener childEventListener;



    public ViewInitiatieveFragment() {
        // Required empty public constructor
    }

    public ViewInitiatieveFragment(InitiativeModel item) {
        super();
        this.item = item;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_view_initiatieve, container, false);
        title = v.findViewById(R.id.title);
        text = v.findViewById(R.id.text);
        final boolean[] isFirst = {true};
        radioGroup = v.findViewById(R.id.radioGroup);

        db = FirebaseDatabase.getInstance().getReference("initiatives/"+item.key+"/answers");
        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull final DataSnapshot dataSnapshot, @Nullable String s) {
                final RadioButton btn = new RadioButton(getContext());
                btn.setButtonDrawable(R.drawable.rb_bg);
                String str = dataSnapshot.child("text").getValue().toString();
                btn.setText(str);
                btn.setTag(new Integer(radioGroup.getChildCount()));
                btn.setTextColor(getContext().getResources().getColor(R.color.colorAccent));
                btn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        String val = dataSnapshot.child("count").getValue().toString();
                        try {
                            int k = Integer.parseInt(val);
                            db.child(""+((Integer)btn.getTag())+"/count").setValue(""+(k+1));
                        } catch (Exception e){
                            db.child(""+((Integer)btn.getTag())+"/count").setValue(""+1);
                            e.printStackTrace();
                        }
                        for (int j = 0; j < radioGroup.getChildCount(); j++){
                            radioGroup.getChildAt(j).setEnabled(false);
                            ((RadioButton)radioGroup.getChildAt(j)).setTextColor(Color.GRAY);
                        }
                        Toast.makeText(getContext(), "Спасибо за ваш голос", Toast.LENGTH_LONG).show();
                    }
                });
                radioGroup.addView(btn);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        title.setText(item.title);
        text.setText(item.text);


        db.addChildEventListener(childEventListener);
        return v;
    }


    @Override
    public String getHeader() {
        return "Инициатива";
    }

    @Override
    public void setOnResumeDelegate(OnResumeDelegate delegate) {
        onResumeDelegate = delegate;
    }

    @Override
    public void onResume() {
        super.onResume();
        onResumeDelegate.onResume("Инициатива");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (db != null){
            db.removeEventListener(childEventListener);
        }
    }
}