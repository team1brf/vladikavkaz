package com.cifra.vladikavkaz.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cifra.vladikavkaz.Adapters.InitiativesAdapter;
import com.cifra.vladikavkaz.Delegate.OnResumeDelegate;
import com.cifra.vladikavkaz.MenuActivity;
import com.cifra.vladikavkaz.Models.InitiativeModel;
import com.cifra.vladikavkaz.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class InitiativesFragment extends FragmentExtanded {
    private String header = "Инициативы";
    private OnResumeDelegate onResumeDelegate;
    private ArrayList<InitiativeModel> initiatives;
    private RecyclerView rvInitiatives;
    private DatabaseReference mDatabase;
    InitiativesAdapter adapter;
    private FloatingActionButton fab ;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_initiative, container, false);
        initiatives = new ArrayList<InitiativeModel>();
//        initiatives.add(getTmpModel());
        adapter = new InitiativesAdapter(initiatives);

        rvInitiatives = view.findViewById(R.id.initiativeRView);
        rvInitiatives.setLayoutManager(new LinearLayoutManager(getContext()));
        rvInitiatives.setAdapter(adapter);

        fab = view.findViewById(R.id.btnAddInitiative);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddInitiativeFragment f = new AddInitiativeFragment();
                ((MenuActivity)getActivity()).push(f,true);
            }
        });

        mDatabase = FirebaseDatabase.getInstance().getReference();
        DatabaseReference db = mDatabase.child("initiatives");
        db.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                InitiativeModel model = dataSnapshot.getValue(InitiativeModel.class);
                model.key = dataSnapshot.getKey();
                initiatives.add(model);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                InitiativeModel model = dataSnapshot.getValue(InitiativeModel.class);
                for (int i = 0; i < initiatives.size(); i++){
                    InitiativeModel m = initiatives.get(i);
                    if (m.text.equals(model.text)) {
                        initiatives.remove(i);
                        initiatives.add(i,model);
                    }
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                InitiativeModel model = dataSnapshot.getValue(InitiativeModel.class);
                for (int i = 0; i < initiatives.size(); i++){
                    InitiativeModel m = initiatives.get(i);
                    if (m.text.equals(model.text)) {
                        initiatives.remove(i);
                    }
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        return view;
    }

//    private InitiativeModel getTmpModel(){
////        InitiativeModel model = new InitiativeModel();
////        model.date = "12.12.1222";
////        model.text = "Какой-то текст";
////        model.title = "Заголовок";
////        return model;
////    }

    @Override
    public String getHeader() {
        return header;
    }

    @Override
    public void setOnResumeDelegate(OnResumeDelegate delegate) {
        onResumeDelegate = delegate;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (onResumeDelegate == null){
            return;
        }
        onResumeDelegate.onResume(header);
    }
}
