package com.cifra.vladikavkaz.Models;

public abstract class MessageItem {

    public static final int TYPE_INCOME = 1;
    public static final int TYPE_OUTCOME = 2;

    abstract public int getType();
}
