package com.cifra.vladikavkaz.Models;

import java.security.PublicKey;

public class NotificationModel {
    public String author;
    public String date;
    public String text;
    public String title;
    public String type;
    public String visible;
}
