package com.cifra.vladikavkaz.Models;

import java.util.List;

public class InitiativeModel {
    public String date;
    public String text;
    public String title;
    public String author;
    public String type;
    public String org;
    public String key;
    public List<QuestionModel> answers;
}
