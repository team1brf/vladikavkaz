package com.cifra.vladikavkaz.Models;

public class RequestModel {
    public String id;
    public String answer;
    public String approved;
    public String author;
    public String date;
    public String organization;
    public String status;
    public String text;
    public String topic;

}
