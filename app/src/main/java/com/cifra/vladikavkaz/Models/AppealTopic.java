package com.cifra.vladikavkaz.Models;

public class AppealTopic {
    public String id;
    public String value;

    public AppealTopic(String id, String value) {
        this.id = id;
        this.value = value;
    }
}
