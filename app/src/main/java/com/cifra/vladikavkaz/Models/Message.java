package com.cifra.vladikavkaz.Models;

public class Message extends MessageItem {

    public String date;
    public String author;
    public String text;
    public int type;

    public String status;
    public String reply;

    private Boolean is_income;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isIs_income() {
        if (is_income == null )
            return false;
        else {
            return is_income;
        }
    }

    public Message dbObject(){
        Message msg = new Message();
        msg.text = text;
        msg.date = date;
        msg.author = author;

        return msg;
    };

    public void setIs_income(boolean is_income) {
        this.is_income = is_income;
    }

    @Override
    public int getType() {
       int _type;
       if(is_income != null && is_income){
           _type = TYPE_INCOME;
       } else {
           _type = TYPE_OUTCOME;
       }
       return _type;
    }
}
