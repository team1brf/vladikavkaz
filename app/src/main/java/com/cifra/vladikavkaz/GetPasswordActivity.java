package com.cifra.vladikavkaz;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cifra.vladikavkaz.Models.CodeResponse;
import com.cifra.vladikavkaz.Retrofit.IJsonApi;
import com.cifra.vladikavkaz.Retrofit.NetworkService;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class GetPasswordActivity extends AppCompatActivity {

    boolean imitateSendMessage = false;
    EditText editTextNumber;
    String phone;
    String code;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_password_activity);

        editTextNumber = findViewById(R.id.etLogin);

        //  Set mask
        MaskedTextChangedListener listener = new MaskedTextChangedListener(
                "+7 ([000]) [000]-[00]-[00]",
                editTextNumber
        );
        editTextNumber.addTextChangedListener(listener);

    }

    public void sendPhoneNumber(View view){
        phone = editTextNumber.getText().toString()
                .replace("(", "")
                .replace(")", "")
                .replace(" ", "")
                .replace("-", "")
                .replace("+", "")
                .substring(1);


        if (imitateSendMessage){
            Intent setCodeActivity = new Intent(this, SetCodeActivity.class);
            setCodeActivity.putExtra("phone", "79094722223");
            setCodeActivity.putExtra("code", "0000");

            startActivity(setCodeActivity);
            finish();
            return;
        }

        NetworkService.getInstance()
                .getJSONApi()
                .SendPhoneNumber(phone)
                .enqueue(new Callback<CodeResponse>() {
                    @Override
                    public void onResponse(Call<CodeResponse> call, Response<CodeResponse> response) {
                        code = response.body().getCode();
                        nextActivity();
                    }

                    @Override
                    public void onFailure(Call<CodeResponse> call, Throwable t) {
                        Log.w("statusOperations", "ERROR");
                    }
                });
    }

    public void nextActivity(){
        Intent setCodeActivity = new Intent(this, SetCodeActivity.class);
        setCodeActivity.putExtra("phone", phone);
        setCodeActivity.putExtra("code", code);

        startActivity(setCodeActivity);
        finish();
    }



}
