package com.cifra.vladikavkaz.Retrofit;

import com.cifra.vladikavkaz.Models.CodeResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IJsonApi {

    @GET("sendSMS.php")
    public Call<CodeResponse> SendPhoneNumber(@Query("phone") String phone);
}
