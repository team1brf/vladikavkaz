package com.cifra.vladikavkaz;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SetCodeActivity extends AppCompatActivity {

    String phone;
    String code;
    EditText etCode;
    SharedPreferences pref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_code_activity);

        etCode = findViewById(R.id.etPassword);
        phone = getIntent().getStringExtra("phone");

        Intent currIntent = getIntent();
        if (!currIntent.hasExtra("code")){
            finish();
        }

        code = currIntent.getStringExtra("code");
        phone = currIntent.getStringExtra("phone");

    }

    public void check(View view){
        String userCode = etCode.getText().toString();
        Toast notify;
        if (userCode.equals(code)){
            CharSequence msg = new StringBuffer("OK");
            notify = Toast.makeText(this,  msg, Toast.LENGTH_LONG);
            saveAuthorizationPreference(phone, code);
            nextActivity();
        }else{
            CharSequence msg = new StringBuffer("Неверный код");
            notify = Toast.makeText(this,  msg, Toast.LENGTH_LONG);
            etCode.setText("");
        }

        notify.show();
    }

    private void saveAuthorizationPreference(String phone, String code){
        if (pref == null){
            pref = PreferenceManager.getDefaultSharedPreferences(this);
        }

        SharedPreferences.Editor edit = pref.edit();

        edit.putString("userPhone", phone);
        edit.putString("userCode", code);

        edit.apply();
    }

    private void nextActivity(){
        Intent menuActivity = new Intent(this, MenuActivity.class);

        menuActivity.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(menuActivity);
        finish();
    }
}
