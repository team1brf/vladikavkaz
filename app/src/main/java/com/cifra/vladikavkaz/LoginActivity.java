package com.cifra.vladikavkaz;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cifra.vladikavkaz.Models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

public class LoginActivity extends AppCompatActivity {

    boolean openLoginActivityDeb = false;
    EditText editTextNumber;
    EditText editTextCode;

    Button btnLogin;
    TextView btnForgetPassword;
    TextView btnRegistration;
    SharedPreferences pref;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (isUserAuthorized() && !openLoginActivityDeb){
            nextActivity();
        }

        setContentView(R.layout.login_activity);

        editTextNumber = findViewById(R.id.etLogin);
        editTextCode = findViewById(R.id.etPassword);

        btnLogin = findViewById(R.id.btnLogin);
        btnForgetPassword = findViewById(R.id.btnForget);
        btnRegistration = findViewById(R.id.btnRegistration);

        //  Set mask
        final MaskedTextChangedListener listener = new MaskedTextChangedListener(
                "+7 ([000]) [000]-[00]-[00]",
                editTextNumber
        );
        editTextNumber.addTextChangedListener(listener);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = editTextNumber.getText().toString()
                        .replace("(", "")
                        .replace(")", "")
                        .replace(" ", "")
                        .replace("-", "")
                        .replace("+", "");
                String code = editTextCode.getText().toString();

                authorization(phone, code);
            }
        });
    }

    public void startGetPasswordActivity(View view){
        Intent regActivity = new Intent(this, GetPasswordActivity.class);
        startActivity(regActivity);
    }

    private void authorization(final String phone, final String code){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users");

        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> ds = dataSnapshot.getChildren();

                String correctCode = "";
                Integer correctCodeDate = -1;

                while (ds.iterator().hasNext()){
                    DataSnapshot dataSnapshotUser = ds.iterator().next();
                    User user = dataSnapshotUser.getValue(User.class);

                    if (user != null && user.date > correctCodeDate)
                        correctCode = user.code;
                }

                if (correctCode.equals(code)){
                    CharSequence msg = new StringBuffer("Успешно");
                    Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
                    toast.show();

                    saveAuthorizationPreference(phone, code);
                    nextActivity();

                }else{
                    CharSequence msg = new StringBuffer(correctCode);
                    Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
                    toast.show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("asdasd", "dasdasd");
            }
        };

        ref.orderByChild("phone").equalTo(phone).addValueEventListener(postListener);
    }

    private void saveAuthorizationPreference(String phone, String code){
        if (pref == null){
            pref = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
        }

        SharedPreferences.Editor edit = pref.edit();

        edit.putString("userPhone", phone);
        edit.putString("userCode", code);

        edit.commit();
    }

    private void nextActivity(){
        Intent menuActivity = new Intent(this, MenuActivity.class);

        menuActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(menuActivity);
        finish();
    }

    private boolean isUserAuthorized(){
        if (pref == null) {
            pref = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
        }

        String code = pref.getString("userCode", null);
        String phone = pref.getString("userPhone", null);

        return code != null && phone != null;
    }


}
