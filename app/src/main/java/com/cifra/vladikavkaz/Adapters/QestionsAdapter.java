package com.cifra.vladikavkaz.Adapters;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cifra.vladikavkaz.Models.QuestionModel;
import com.cifra.vladikavkaz.R;

import java.util.ArrayList;

public class QestionsAdapter extends RecyclerView.Adapter<QestionsAdapter.QuestionsViewHolder> {

    public ArrayList<QuestionModel> items;

    public  QestionsAdapter(ArrayList<QuestionModel> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public QestionsAdapter.QuestionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_item, parent, false);
        return new QuestionsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final QestionsAdapter.QuestionsViewHolder holder, final int position) {
        final QuestionModel mdl = items.get(position);
        holder.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                items.remove(position);
                QestionsAdapter.this.notifyDataSetChanged();
            }
        });
        holder.etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mdl.text = holder.etName.getText().toString();
            }
        });

        holder.etName.setText(mdl.text);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class QuestionsViewHolder extends RecyclerView.ViewHolder {
        EditText etName;
        Button btnRemove;

        public QuestionsViewHolder(@NonNull View itemView) {
            super(itemView);
            etName = itemView.findViewById(R.id.etName);
            btnRemove = itemView.findViewById(R.id.removeBtn);
        }
    }
}
