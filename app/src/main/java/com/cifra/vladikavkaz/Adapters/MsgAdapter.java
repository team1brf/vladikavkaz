package com.cifra.vladikavkaz.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cifra.vladikavkaz.Models.Message;
import com.cifra.vladikavkaz.Models.MessageItem;
import com.cifra.vladikavkaz.R;

import java.util.List;

public class MsgAdapter extends RecyclerView.Adapter<MsgAdapter.ViewHolder>{

    private Context context;
    private List<Message> list;

    public MsgAdapter(Context context, List<Message> list) {
        this.context = context;
        this.list = list;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType){
            case MessageItem.TYPE_INCOME:
                view = LayoutInflater.from(context).inflate(R.layout.simple_income_message, parent, false);
                return new ViewHolder(view);

            case MessageItem.TYPE_OUTCOME:
                view = LayoutInflater.from(context).inflate(R.layout.simple_outcome_message, parent, false);
                return new ViewHolder(view);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position).getType();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Message message = (Message) list.get(position);
        holder.message.setText(message.getText());

    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView message;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            message = itemView.findViewById(R.id.tvMessage);
        }
    }
}