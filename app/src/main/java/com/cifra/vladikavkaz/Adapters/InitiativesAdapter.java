package com.cifra.vladikavkaz.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cifra.vladikavkaz.Fragments.ViewInitiatieveFragment;
import com.cifra.vladikavkaz.MenuActivity;
import com.cifra.vladikavkaz.Models.InitiativeModel;
import com.cifra.vladikavkaz.R;

import java.util.ArrayList;

public class InitiativesAdapter extends RecyclerView.Adapter<InitiativesAdapter.InitiativesViewHolder>{
    private ArrayList<InitiativeModel> initiatives;
    Context context;

    public InitiativesAdapter(ArrayList<InitiativeModel> initiatives) {
        this.initiatives = initiatives;
    }


    @NonNull
    @Override
    public InitiativesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.simple_initiative_item, parent, false);
        return new InitiativesAdapter.InitiativesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InitiativesViewHolder holder, final int position) {
        InitiativeModel initiative = initiatives.get(position);
        holder.tvDate.setText(initiative.date);
        holder.tvText.setText(initiative.text);
        View.OnClickListener l = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MenuActivity)context).push(new ViewInitiatieveFragment(initiatives.get(position)), true);
            }
        };
        holder.tvDate.setOnClickListener(l);
        holder.tvText.setOnClickListener(l);
        holder.v.setOnClickListener(l);
    }

    @Override
    public int getItemCount() {
        return initiatives.size();
    }

    class InitiativesViewHolder extends RecyclerView.ViewHolder{

        TextView tvText;
        TextView tvDate;
        View v;


        InitiativesViewHolder(@NonNull View itemView) {
            super(itemView);
            v = itemView;
            tvDate = itemView.findViewById(R.id.date);
            tvText = itemView.findViewById(R.id.header);
        }
    }
}
