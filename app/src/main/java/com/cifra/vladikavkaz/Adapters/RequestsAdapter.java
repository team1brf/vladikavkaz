package com.cifra.vladikavkaz.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cifra.vladikavkaz.Fragments.RequestDetailFragment;
import com.cifra.vladikavkaz.MenuActivity;
import com.cifra.vladikavkaz.Models.RequestModel;
import com.cifra.vladikavkaz.R;

import java.util.ArrayList;

public class RequestsAdapter extends RecyclerView.Adapter<RequestsAdapter.RequestsViewHolder> {
    ArrayList<RequestModel> requests;
    Context context;

    public RequestsAdapter(ArrayList<RequestModel> requests, Context context) {
        this.requests = requests;
        this.context = context;
    }

    @NonNull
    @Override
    public RequestsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.simple_request_item, parent, false);
        return new RequestsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestsViewHolder holder, int position) {
        final RequestModel model = requests.get(position);

        holder.tvStatus.setText(model.status);
        holder.tvText.setText(model.text);
        holder.tvTopic.setText(model.topic);
        holder.tvDate.setText(model.date);

        if (model.approved.equals("true")){
            holder.ivIcon.setImageDrawable(context.getDrawable(R.mipmap.approved_icon));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MenuActivity)context).push(new RequestDetailFragment(model), true);
            }
        });

    }

    @Override
    public int getItemCount() {
        return requests.size();
    }

    class RequestsViewHolder extends RecyclerView.ViewHolder{

        TextView tvDate;
        TextView tvTopic;
        TextView tvText;
        TextView tvStatus;
        ImageView ivIcon;

        RequestsViewHolder(@NonNull View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.dateValue);
            tvTopic = itemView.findViewById(R.id.topicValue);
            tvText = itemView.findViewById(R.id.textValue);
            tvStatus = itemView.findViewById(R.id.statusValue);
            ivIcon = itemView.findViewById(R.id.ivIcon);
        }
    }
}
