package com.cifra.vladikavkaz.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cifra.vladikavkaz.Fragments.NotificatonDetailFragment;
import com.cifra.vladikavkaz.MenuActivity;
import com.cifra.vladikavkaz.Models.NotificationModel;
import com.cifra.vladikavkaz.R;

import java.util.ArrayList;
import java.util.zip.Inflater;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.NotificationsViewHolder> {
    private ArrayList<NotificationModel> notifications;
    private Context context;

    public NotificationsAdapter(ArrayList<NotificationModel> notifications, Context context) {
        this.notifications = notifications;
        this.context = context;
    }

    @NonNull
    @Override
    public NotificationsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.simple_notification_item, parent, false);
        return new NotificationsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationsViewHolder holder, int position) {
        final NotificationModel notification = notifications.get(position);

        holder.tvDate.setText(notification.date);
        holder.tvTitle.setText(notification.title);

        if (!notification.type.equals("alert")){
            holder.image.setImageDrawable(context.getDrawable(R.mipmap.help));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MenuActivity)context).push(new NotificatonDetailFragment(notification), true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    class NotificationsViewHolder extends RecyclerView.ViewHolder{
        TextView tvTitle;
        TextView tvDate;
        ImageView image;

        NotificationsViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.title);
            tvDate = itemView.findViewById(R.id.date);
            image = itemView.findViewById(R.id.ivIcon);
        }
    }
}
