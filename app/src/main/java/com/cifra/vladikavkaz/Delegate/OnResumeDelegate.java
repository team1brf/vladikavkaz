package com.cifra.vladikavkaz.Delegate;

public abstract class OnResumeDelegate {
    public abstract void onResume(String header);
}
